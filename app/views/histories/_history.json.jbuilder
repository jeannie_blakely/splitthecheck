json.extract! history, :id, :will_split, :wont_split, :user_id, :restaurant_id, :created_at, :updated_at
json.url history_url(history, format: :json)
