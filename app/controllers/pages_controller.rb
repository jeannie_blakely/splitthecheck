class PagesController < ApplicationController
  before_action :authenticate_user!, :except => [:about]

  def about
  end

  def summary
    @user = current_user
  end
end
