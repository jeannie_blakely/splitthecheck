class FavoritesController < ApplicationController
  before_action :authenticate_user!
  def favorite
    @user = current_user
    @restaurant = Restaurant.find(params[:restaurant_id])
    @user.favorite!(@restaurant)
    respond_to do |format|
      format.html { redirect_to restaurants_url }
      format.json { head :no_content }
    end
  end
end
