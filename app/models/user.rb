class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :histories, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :favorites, dependent: :destroy
  has_many :restaurants, through: :favorites

  def favorite!(restaurant)
    self.favorites.create!(restaurant_id: restaurant.id)
  end

  def favorite?(restaurant)
    self.favorites.find_by_restaurant_id(restaurant.id)
  end
end
