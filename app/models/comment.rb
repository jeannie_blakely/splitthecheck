class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :restaurant

  def self.search(restaurant_id)
    if restaurant_id
      where('restaurant_id LIKE ?', restaurant_id)
    end
  end
end
