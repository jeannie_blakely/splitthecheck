class Restaurant < ApplicationRecord
  has_many :histories, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :favorites, dependent: :destroy
  has_many :users, through: :favorites
  validates :name, :location, presence: true
  validates :name, uniqueness: true
  
  def will_split_vote(user)
    #self.increment! :will_split
    @history = self.histories.create(will_split: 1, user: user)
  end

  def wont_split_vote(user)
    #self.increment! :wont_split
    @history = self.histories.create(wont_split: 1, user: user)
  end

  def self.search(term)
    if term
      where('name LIKE ? OR location LIKE ?', "%#{term}%", "%#{term}%")
    else
      all.sort_by(&:composite_vote).reverse
      #all
    end
  end

  def composite_vote
    if self.histories.sum(:will_split) + self.histories.sum(:wont_split) == 0
      0
    else
      self.histories.sum(:will_split).to_f/(self.histories.sum(:will_split) + self.histories.sum(:wont_split))
    end
  end
   
end
