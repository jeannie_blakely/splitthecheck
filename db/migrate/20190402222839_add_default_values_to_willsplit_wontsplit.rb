class AddDefaultValuesToWillsplitWontsplit < ActiveRecord::Migration[5.2]
  def up
    change_column :restaurants, :will_split, :integer, default: 0
    change_column :restaurants, :wont_split, :integer, default: 0   
  end

  def down
    change_column :restaurants, :will_split, :integer, default: nil
    change_column :restaurants, :wont_split, :integer, default: nil  
  end
end
