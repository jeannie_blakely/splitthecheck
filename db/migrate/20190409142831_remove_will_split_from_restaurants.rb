class RemoveWillSplitFromRestaurants < ActiveRecord::Migration[5.2]
  def change
    remove_column :restaurants, :will_split, :integer
  end
end
