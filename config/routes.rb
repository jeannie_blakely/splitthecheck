Rails.application.routes.draw do
  resources :comments, only: [:index, :show, :create, :new]
  resources :histories, only: [:index, :show]
  devise_for :users 
  resources :restaurants, except: [:destroy] do
      put :upvote
      put :downvote
      resources :favorites, only: [:create]
    end
  root 'restaurants#index'
  get '/about' => 'pages#about'
  get '/summary' => 'pages#summary'
  match 'favorite', to: 'favorites#favorite', via: :post
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
