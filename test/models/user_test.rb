require 'test_helper'

class UserTest < ActiveSupport::TestCase
  setup do
    @user = users(:one)
    @user2 = users(:two)
    @restaurant = restaurants(:one)
    @restaurant2 = restaurants(:two)
  end

  test "creates favorite for restaurant" do
    assert_difference('Favorite.count') do
      @user.favorite!(@restaurant)
    end
  end

  test "creates favorite for restaurant not favorited by user" do
    @user.favorite!(@restaurant)
    @user2.favorite!(@restaurant2)
    assert_difference('Favorite.count', 2) do
      @user2.favorite!(@restaurant)
      @user.favorite!(@restaurant2)
    end
  end
  
  test "favorite? returns false for restaurant not favorited by user" do   
    refute(@user.favorite?(@restaurant))
  end

  test "favorite? returns true for restaurant favorited by user" do
    @user.favorite!(@restaurant)
    assert(@user.favorite?(@restaurant))
  end

end
