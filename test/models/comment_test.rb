require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  setup do
    @restaurant = restaurants(:one)
    @restaurant2 = restaurants(:two)
  end

   test "search returns matches on comment for same restaurant" do
    result = Comment.new
    result = Comment.search(@restaurant.id)
    assert_equal(2, result.count)
    assert_equal("CommentThree", result.first.comment)
    assert_equal("CommentOne", result.last.comment)
  end
  
  test "search returns matches on comment for another restaurant" do
    result = Comment.new
    result = Comment.search(@restaurant2.id)
    assert_equal(1, result.count)
    assert_equal("CommentTwo", result.first.comment)
  end
end
