require 'test_helper'

class RestaurantTest < ActiveSupport::TestCase
  setup do
    @restaurant = restaurants(:one)
    @history = histories(:one)
    @user = users(:one)
    @name = "Paneras"
    @location = "South"
  end

  test "restaurant name and location may not be empty" do
    restaurant = Restaurant.new
    assert restaurant.invalid? 
    assert restaurant.errors[:name].any?
    assert restaurant.errors[:location].any?
  end

  test "restaurant is not valid without a unique name" do
    restaurant = Restaurant.new(name: restaurants(:one).name,
                                location: "Shorter")
    assert restaurant.invalid?
    assert_equal ["has already been taken"], restaurant.errors[:name]
  end
  
  test "will_split_vote method increments will_split by 1" do
    assert_difference('@restaurant.histories.sum(:will_split)') do
      @restaurant.will_split_vote(@user)
    end
  end

  test "wont_split_vote increments wont_split by 1" do
    assert_difference('@restaurant.histories.sum(:wont_split)') do
      @restaurant.wont_split_vote(@user)
    end
  end

  test "search returns matches on name and location" do
    @restaurant = restaurants(:two)
    @restaurant = restaurants(:search)
    result = Restaurant.new
    result = Restaurant.search("Panera")
    assert_equal(result.first.name, "Paneras")
    assert_equal(result.first.location, "South St")
  end
  
  test "composite vote returns will_split divided by wont_split + wont_split" do
    @history = histories(:two)
    #assert_equal(2, @restaurant.histories.count)
    assert_equal(0.5, @restaurant.composite_vote)
    2.times { @restaurant.will_split_vote(@user) }
    assert_equal(0.75, @restaurant.composite_vote)
    18.times { @restaurant.will_split_vote(@user) }
    4.times { @restaurant.wont_split_vote(@user) }
    assert_in_delta(0.807, @restaurant.composite_vote, 0.001)
  end

end
