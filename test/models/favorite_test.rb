require 'test_helper'

class FavoriteTest < ActiveSupport::TestCase
  setup do
    @user = users(:one)
    @restaurant = restaurants(:one)
    @user.favorite!(@restaurant)
  end

  test "favorite is not valid without a unique user restaurant combo" do
    favorite = Favorite.new(user: @user, restaurant: @restaurant)
    favorite.save
    assert favorite.invalid?
    assert favorite.errors.any?
  end
end
