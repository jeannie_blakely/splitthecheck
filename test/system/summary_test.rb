require "application_system_test_case"

class SummaryTest < ApplicationSystemTestCase
  setup do
    @user = User.new(email: "user@example.com",
                     encrypted_password: Devise::Encryptor.digest(User, 'password'))

    @user.save
    Restaurant.delete_all
    @restaurant = Restaurant.new(name: "Paneras",
                                location: "Shorter")
    @restaurant.save
    @comment = comments(:one)
    @comment.save
    @histories = histories(:one)
    @histories.save
    #@user.favorite!(@restaurant)
  end


  test "user summary" do
    visit summary_url
    assert_selector "h2", text: "Log in"
    fill_in "Email", with: "user@example.com"
    fill_in "Password", with: "password"
    #click_on "Log in"
    #assert_selector "h2", text: "User Summary"
  end

end
