require "application_system_test_case"

class CommentsTest < ApplicationSystemTestCase
  setup do
    @user = User.new(email: "user@example.com",
                     encrypted_password: Devise::Encryptor.digest(User, 'password'))

    @user.save
    Restaurant.delete_all
    @restaurant = Restaurant.new(name: "Paneras",
                                location: "Shorter")
    @restaurant.save
    @comment = comments(:one)
  end


  test "creating a Comment" do
    visit restaurants_url
    first('.restaurant').click_on 'Show'
    assert_no_text 'New Comment'
    assert_text 'Back'
    click_link('Login')
    assert_selector "h2", text: "Log in"
    fill_in "Email", with: "user@example.com"
    fill_in "Password", with: "password"
    click_on "Log in"
    first('.restaurant').click_on 'Show'
    assert_text 'Add Comment'
    click_on "Add Comment"
    fill_in "Comment", with: @comment.comment
    click_on "Create Comment"
    assert_text "Comment was successfully created"
    assert_selector "h1", text: "Comment"
  end

end
