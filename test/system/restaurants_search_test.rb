require "application_system_test_case"

class RestaurantsSearchTest < ApplicationSystemTestCase


  test "searching name returns results" do

    visit restaurants_url
    restaurants = Restaurant.all
    assert_equal 4, restaurants.size
    fill_in 'search_term', with: 'paneras'
    first('.search').click_on 'Search'
    assert_selector ".restaurants td", text: "Paneras"
  end

  test "searching location returns results" do

    visit restaurants_url
    restaurants = Restaurant.all
    assert_equal 4, restaurants.size
    fill_in 'search_term', with: 'south'
    first('.search').click_on 'Search'
    assert_selector ".restaurants td", text: "South"
  end
end
