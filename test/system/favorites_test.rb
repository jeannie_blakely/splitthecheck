require "application_system_test_case"

class FavoritesTest < ApplicationSystemTestCase
  setup do
    @user = User.new(email: "user@example.com",
                     encrypted_password: Devise::Encryptor.digest(User, 'password'))

    @user.save
    Restaurant.delete_all
    @restaurant = Restaurant.new(name: "Paneras",
                                location: "Shorter")
    @restaurant.save
  end


  test "clicking Favorite creates favorite for user restaurant combination" do
    visit restaurants_url
    first('.restaurant').assert_no_text 'Favorite'
    visit restaurants_url
    first('.search').click_on "Login"
    assert_selector "h2", text: "Log in"
    fill_in "Email", with: "user@example.com"
    fill_in "Password", with: "password"
    click_on "Log in"
    first('.restaurant').click_on 'Favorite'
    first('.restaurant').assert_text 'Favorited!'
  end

end
