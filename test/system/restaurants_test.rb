require "application_system_test_case"

class RestaurantsTest < ApplicationSystemTestCase
  setup do
    @user = User.new(email: "user@example.com",
                     encrypted_password: Devise::Encryptor.digest(User, 'password'))

    @user.save
    Restaurant.delete_all
    @restaurant = Restaurant.new(name: "Paneras",
                                location: "Shorter")
    @restaurant.save
  end

  
  test "clicking will split votes button increments votes by 1" do
    visit restaurants_url
    assert_equal '0', @restaurant.histories.sum(:will_split).to_s
    first('.restaurant').click_on 'will split votes:'
    assert_selector "h2", text: "Log in"
    fill_in "Email", with: "user@example.com"
    fill_in "Password", with: "password"
    click_on "Log in"
    assert_equal '0', @restaurant.histories.sum(:will_split).to_s
    first('.restaurant').click_on 'will split votes:'
    restaurants = Restaurant.all
    assert_equal 1, restaurants.first.histories.sum(:will_split)

  end

  test "clicking wont split votes button increments votes by 1" do
    visit restaurants_url
    assert_equal '0', @restaurant.histories.sum(:wont_split).to_s
    first('.restaurant').click_on 'wont split votes:'
    assert_selector "h2", text: "Log in"
    fill_in "Email", with: "user@example.com"
    fill_in "Password", with: "password"
    click_on "Log in"
    assert_equal '0', @restaurant.histories.sum(:wont_split).to_s
    first('.restaurant').click_on 'wont split votes:'
    first('.restaurant').click_on 'wont split votes:'
    restaurants = Restaurant.all
    assert_equal 2, restaurants.first.histories.sum(:wont_split)

  end

end
