require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  setup do
    @user = users(:one)
  end

  test "should get about" do
    get about_url
    assert_response :success
  end

  test "should redirect to login for summary page when not logged in" do
    get summary_url
    assert_redirected_to new_user_session_path
  end
 
  test "should get summary page when logged in" do
    sign_in @user
    get summary_url
    assert_response :success
  end

end
