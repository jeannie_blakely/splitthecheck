require 'test_helper'

class RestaurantsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  setup do
    @user = users(:one)
    @restaurant = restaurants(:one)
    @name = "Paneras"
  end

  test "should get index" do
    get restaurants_url
    assert_response :success
  end

  test "should get new if signed in" do
    sign_in @user
    get new_restaurant_url
    assert_response :success
  end

  test "should get redirected to login on new if not signed in" do
    get new_restaurant_url
    assert_redirected_to new_user_session_path
  end

  test "should create restaurant when signed in" do
    sign_in @user
    assert_difference('Restaurant.count') do
      post restaurants_url, params: { restaurant: { 
        location: @restaurant.location, 
        name: @restaurant
        } 
      }
    end

    assert_redirected_to restaurant_url(Restaurant.last)
  end

  test "should not create restaurant and be directed to login when not signed in" do
    assert_difference('Restaurant.count', 0) do
      post restaurants_url, params: { restaurant: { 
        location: @restaurant.location, 
        name: @restaurant
        } 
      }
    end

    assert_redirected_to new_user_session_path
  end

  test "should show restaurant" do
    get restaurant_url(@restaurant)
    assert_response :success
  end

  test "should get edit" do
    sign_in @user
    get edit_restaurant_url(@restaurant)
    assert_response :success
  end

  test "should not get edit if not logged in" do
    get edit_restaurant_url(@restaurant)
    assert_redirected_to new_user_session_path
  end

  test "should update restaurant" do
    sign_in @user
    patch restaurant_url(@restaurant), params: { restaurant: { location: @restaurant.location, name: @restaurant.name } }
    assert_redirected_to restaurant_url(@restaurant)
  end

  test "should not update restaurant and redirect to login if not signed in" do
    patch restaurant_url(@restaurant), params: { restaurant: { location: @restaurant.location, name: @restaurant.name } }
    assert_redirected_to new_user_session_path
  end

  test "should redirect after put upvote if logged in" do
    sign_in @user
    put restaurant_upvote_path(@restaurant)
    assert_redirected_to restaurants_url
  end

  test "should redirect to login after put upvote if not logged in" do
    put restaurant_upvote_path(@restaurant)
    assert_redirected_to new_user_session_path
  end

  test "should redirect after put downvote if logged in" do
    sign_in @user
    put restaurant_upvote_path(@restaurant)
    assert_redirected_to restaurants_url
  end

  test "should redirect to login after put downvote if not logged in" do
    put restaurant_upvote_path(@restaurant)
    assert_redirected_to new_user_session_path
  end

  # modified to assert raised error when restricted in route.rb
  test "should not destroy restaurant" do
    assert_raises(ActionController::RoutingError) do
      assert_difference('Restaurant.count', -1) do
        delete restaurant_url(@restaurant)
    end

      assert_redirected_to restaurants_url
    end
  end
end
