require 'test_helper'

class FavoritesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  setup do
    @user = users(:one)
    @restaurant = restaurants(:one)
  end

  test "should redirect after post favorite if logged in" do
    sign_in @user
    post favorite_path(restaurant_id: @restaurant.id)
    assert_redirected_to restaurants_url
  end

  test "should redirect to login after post favorite if not logged in" do
    post favorite_path(restaurant_id: @restaurant.id)
    assert_redirected_to new_user_session_path
  end
end
