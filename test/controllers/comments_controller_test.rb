require 'test_helper'

class CommentsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  setup do
    @user = users(:one)
    @comment = comments(:one)
  end

  test "should get index" do
    get comments_url
    assert_response :success
  end

  test "should get new if logged in" do
    sign_in @user
    #restaurant_url(@comment.restaurant_id)
    #assert_response :success
  end
 
  test "should get redirected to login on new if not signed in" do
    get new_comment_url
    assert_redirected_to new_user_session_path
  end

  test "should create comment if logged in" do
    sign_in @user
    assert_difference('Comment.count') do
      post comments_url, params: { comment: { comment: @comment.comment, restaurant_id: @comment.restaurant_id, user_id: @comment.user_id } }
    end

    assert_redirected_to restaurant_url(@comment.restaurant.id)
  end

  test "should not create comment and be directed to login when not signed in" do
    assert_difference('Comment.count', 0) do
      post comments_url, params: { comment: { comment: @comment.comment, restaurant_id: @comment.restaurant_id, user_id: @comment.user_id } }
    end

    assert_redirected_to new_user_session_path
  end

  test "should show comment" do
    get comment_url(@comment)
    assert_response :success
  end

end
